package com.notesonjava.spanish;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.notesonjava.auth.QuizzAccessManager;
import com.notesonjava.auth.UserHandler;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.conjugation.ConjugationAnswerRepository;
import com.notesonjava.spanish.conjugation.ConjugationReportController;
import com.notesonjava.spanish.conjugation.ConjugationReportService;

import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJackson;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReportApplication {
	
	public static String CONJUGATION_QUEUE = "AnswersConjugations";
	public static String VERB_QUEUE = "AnswersVerbs";


	public static String remoteConnectionString(String user, String password, String host, String db) {
		return "mongodb+srv://"+user+":"+password+"@"+host+"/"+db;	
	}
	
	public static String localConnectionString(String host, int port) {
		return "mongodb://"+host+":"+port;
	}

	public static void main(String[] args) throws IOException, TimeoutException {
		PropertyMap prop = PropertyLoader.loadProperties();
		String portValue = prop.get("server.port").orElse("8080");
		String mongoPort = prop.get("mongo.port").orElse("27017");
		String mongoHost = prop.get("mongo.host").orElse("localhost");
		String mongoDb = prop.get("mongo.db").orElse("answers");
		String authUserUrl = prop.get("auth.user.url").orElse("https://fmottard.auth0.com/userinfo");
		int dbPort = Integer.parseInt(mongoPort);
		
		
		MongoClient client = MongoClients.create(localConnectionString(mongoHost, dbPort));
		MongoDatabase database = client.getDatabase(mongoDb);
		
		ConjugationAnswerRepository conjugationRepo = new ConjugationAnswerRepository(database);
    
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		JavalinJackson.configure(mapper);
		
		UserHandler userHandler = new UserHandler();
		
		
		ConjugationReportService conjugationService = new ConjugationReportService(conjugationRepo);
		ConjugationReportController conjugationController = new ConjugationReportController(conjugationService, mapper, userHandler);
		
		QuizzAccessManager accessManager = new QuizzAccessManager(authUserUrl, mapper);
		
		
		Javalin app = Javalin.create(config -> {
			config.defaultContentType = "application/json;charset=utf-8";
			config.requestLogger((ctx, timeMs) -> {
			    log.info(ctx.method() + " "  + ctx.path() + " took " + timeMs + " ms");
			});
			config.enableCorsForAllOrigins();
			config.accessManager((handler, ctx, permittedRoles) -> accessManager.manage(handler, ctx, permittedRoles));
		});
				
		
		
		app.get("/healthz", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				log.info("User: " + user.get());
			} else {
				log.info("Missing user");
			}
			ctx.result("I am alive : " +user.orElse("Missing User"));
		});
		
		
		conjugationController.init(app);
		
		app.start(Integer.parseInt(portValue));
		
	}
}

