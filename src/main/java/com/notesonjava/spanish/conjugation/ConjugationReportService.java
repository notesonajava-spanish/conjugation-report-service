package com.notesonjava.spanish.conjugation;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.notesonjava.spanish.DocumentAdapter;
import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.ConjugationAnswer;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.WordStats;

import io.reactivex.Flowable;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ConjugationReportService {
	
	
	private ConjugationAnswerRepository conjugationAnswerRepo;
	
	public List<String> getImportantVerbs() {
		return Arrays.asList("estar", "ser", "tener", "poner", "poder", "hacer", "dar", "caer", "haber", "traer", "caber", "prestar", "llevar", "llover", "llorar");
	}
	
	
	public Flowable<WordStats<Conjugation>> conjugationReport(String username){
		Map<Conjugation, Collection<ConjugationAnswer>> answers = Flowable.fromPublisher(conjugationAnswerRepo.findByUsername(username))
							.map(doc -> DocumentAdapter.toConjugationAnswer(doc))
							.toMultimap(answer -> answer.getConjugation())
							.blockingGet();
		
		return Flowable.fromIterable(answers.entrySet())
				.map(entry -> createStats(entry.getKey(), entry.getValue()));
	}

	public Flowable<WordStats<String>> verbReport(String username){
		Map<String, Collection<ConjugationAnswer>> answers = Flowable.fromPublisher(conjugationAnswerRepo.findByUsername(username))
							.map(doc -> DocumentAdapter.toConjugationAnswer(doc))
							.toMultimap(answer -> answer.getConjugation().getVerb())
							.blockingGet();
		
		return Flowable.fromIterable(answers.entrySet())
				.map(entry -> createStats(entry.getKey(), entry.getValue()));
	}
	
	
	public Flowable<WordStats<Tiempo>> conjugationTiempoReport(String username){
		
		Map<Tiempo, Collection<ConjugationAnswer>> answers = Flowable.fromPublisher(conjugationAnswerRepo.findByUsername(username))
				.map(doc -> DocumentAdapter.toConjugationAnswer(doc))
				.toMultimap(answer -> answer.getConjugation().getTiempo())
				.blockingGet();
		
		return Flowable.fromIterable(answers.entrySet())
			.map(entry -> createStats(entry.getKey(), entry.getValue()));
		
	}
	
	public Flowable<WordStats<LocalDate>> getLastWeekReport(String username){		
		ZonedDateTime refDate= ZonedDateTime.now().minusWeeks(1).truncatedTo(ChronoUnit.DAYS);
		return getDailyReport(username, refDate);					
	}
	
	public Flowable<WordStats<LocalDate>> getDailyReport(String username, ZonedDateTime refDate){				
		Map<LocalDate, Collection<ConjugationAnswer>> map  = Flowable.fromPublisher(conjugationAnswerRepo.findByAnswerTimeAfterAndUsername(refDate, username))
						.doOnNext(System.out::println)
						.map(doc -> DocumentAdapter.toConjugationAnswer(doc))
						.toMultimap(ConjugationAnswer::getAnswerDay)
						.blockingGet();
		return Flowable.fromIterable(map.entrySet()).map(entry -> createStats(entry.getKey(), entry.getValue()));					
	}
	
	
	
	private <T> WordStats<T>  createStats(T key, Collection<ConjugationAnswer> answers) {
		WordStats<T> current = new WordStats<>(key);
		answers.forEach(answer -> {
			current.addAttempt();
			if(answer.getScore() == 1) {
				current.addValid();
			}
		});
		return current;
	}
	
}