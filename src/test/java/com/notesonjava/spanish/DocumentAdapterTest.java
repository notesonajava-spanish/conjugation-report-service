package com.notesonjava.spanish;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.bson.BsonDateTime;
import org.bson.Document;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.ConjugationAnswer;
import com.notesonjava.spanish.model.Persona;
import com.notesonjava.spanish.model.Tiempo;

@Tag("test")
public class DocumentAdapterTest {

	
	@Test
	public void convertConjugationAnswer() {
		Conjugation c = new Conjugation();
		c.setPersona(Persona.EL);
		c.setTiempo(Tiempo.ImperativoPresente);
		c.setWord("word");
		c.setVerb("verb");
		ConjugationAnswer answer = new ConjugationAnswer();
		answer.setAnswer("answer");
		answer.setAnswerTime(ZonedDateTime.of(2018, 11, 21, 14, 30, 15, 0, ZoneOffset.UTC));
		answer.setUsername("username");
		answer.setScore(10);
		answer.setConjugation(c);
		Document doc = DocumentAdapter.toDocument(answer);
		BsonDateTime answerTime = (BsonDateTime) doc.get("answerTime");
		Date answerDate = new Date(answerTime.getValue());
		doc.replace("answerTime", answerDate);
		ConjugationAnswer result = DocumentAdapter.toConjugationAnswer(doc);
		Assertions.assertThat(result).isEqualTo(answer);	
	}

	
	@Test
	public void convertAnswerDocument() {
		Document doc = Document.parse(" { \"_id\" : { \"$oid\" : \"5cacddf96b6afc7a28b2b3c1\" }, \"conjugation\" : { \"word\" : \"oblige\", \"tiempo\" : \"IndicativoPreteritoPerfectoSimple\", \"persona\" : \"IndicativoPreteritoPerfectoSimple\", \"verb\" : \"obligar\" }, \"answer\" : \"obligo\", \"username\" : \"auth0|5bec6dda5c45c255a8544a40\", \"answerTime\" : { \"$date\" : 1554832846827 } }");
		ConjugationAnswer answer = DocumentAdapter.toConjugationAnswer(doc);
		Assertions.assertThat(answer.getAnswer()).isEqualTo("abcd");
	}
	
	@Test
	public void convertConjugationDocument() {
		Document doc = Document.parse("{ \"word\" : \"oblige\", \"tiempo\" : \"IndicativoPreteritoPerfectoSimple\", \"persona\" : \"IndicativoPreteritoPerfectoSimple\", \"verb\" : \"obligar\" }");
		Conjugation conjugation = DocumentAdapter.toConjugation(doc);
		Assertions.assertThat(conjugation.getVerb()).isEqualTo("obliger");
	}
}
