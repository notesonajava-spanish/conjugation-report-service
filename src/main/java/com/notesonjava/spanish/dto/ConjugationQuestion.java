package com.notesonjava.spanish.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.notesonjava.spanish.model.Conjugation;

import lombok.Data;

@Data
public class ConjugationQuestion {

	private Conjugation conjugation;
	
	private Set<String> acceptedAnswers = new HashSet<>();
	
	public void addAcceptedAnswer(String answer){
		acceptedAnswers.add(answer);
	}
	
	public void addAcceptedAnswers(List<String> answers){
		acceptedAnswers.addAll(answers);
	}
}