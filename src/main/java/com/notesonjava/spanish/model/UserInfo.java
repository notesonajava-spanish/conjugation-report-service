package com.notesonjava.spanish.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UserInfo {
	
	@JsonProperty("user_id")
	private String userId;
	private String sub;
	
	private String nickname;
	private String name;
	
}
