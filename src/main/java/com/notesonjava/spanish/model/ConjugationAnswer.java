package com.notesonjava.spanish.model;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConjugationAnswer {
	
	private Conjugation conjugation;
	
	private String username;
	
	private String answer;
	
	private ZonedDateTime answerTime;
	
	private int score;
	
	public ConjugationAnswer(Conjugation c, String username){
		this.conjugation = c;
		this.setUsername(username);
		this.setAnswerTime(ZonedDateTime.now(ZoneOffset.UTC));
	}
	
	@JsonIgnore
	public LocalDate getAnswerDay(){
		return answerTime.toLocalDate();
	}	
}
