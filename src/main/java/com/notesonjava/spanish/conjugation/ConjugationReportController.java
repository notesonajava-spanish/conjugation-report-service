package com.notesonjava.spanish.conjugation;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.auth.UserHandler;
import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.WordStats;

import io.javalin.Javalin;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class ConjugationReportController {

	private ConjugationReportService conjugationService;
	private ObjectMapper mapper;
	
	
	private UserHandler userHandler;
	
	public void init(Javalin app) {
		
		app.get("/word", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				List<WordStats<Conjugation>> result = conjugationService.conjugationReport(user.get()).toList().blockingGet();
				ctx.status(200);
				log.info("Result : " + result.toString());
				ctx.result(mapper.writeValueAsString(result));
			}
		});
		
		app.get("/daily", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				List<WordStats<LocalDate>> result = conjugationService.getLastWeekReport(user.get()).toList().blockingGet();
				ctx.status(200);
				ctx.result(mapper.writeValueAsString(result));	
			}
		});
		
		app.get("/tiempos", ctx ->{
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {	
				List<WordStats<Tiempo>> result = conjugationService.conjugationTiempoReport(user.get()).toList().blockingGet();
				ctx.status(200);
				ctx.result(mapper.writeValueAsString(result));
			}
		});

	}
}
