package com.notesonjava.spanish.extensions;

import java.util.Scanner;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NativeAppExtension implements BeforeAllCallback, AfterAllCallback{

	private Process pr;
	private Scanner scanner;
	private AtomicBoolean terminate = new AtomicBoolean(false);

	
	@Override
	public void afterAll(ExtensionContext context) throws Exception {
		log.info("Closing info");
		terminate.set(true);
		Flowable.timer(1, TimeUnit.SECONDS);
		pr.destroy();
		scanner.close();		
	}

	@Override
	public void beforeAll(ExtensionContext context) throws Exception {
		System.out.println("");
		final AtomicBoolean ready = new AtomicBoolean(false);
		
		
		ProcessBuilder pb = new ProcessBuilder("./transaction-service-javalin", "-Dmongo_port=27018", "-Dserver_port=7001", "-Dorg.slf4j.simpleLogger.log.com.notesonjava.config=warn");
		pb.redirectErrorStream(true);
		
		pr = pb.start();

		
		scanner = new Scanner(pr.getInputStream());
		
		Observable<String> serverFlow = Observable.create(source ->  {
			
			Flowable.interval(100, 10, TimeUnit.MILLISECONDS).takeWhile(value -> !terminate.get()).forEach(value -> { 
				Future<Boolean> result = Single.fromCallable(() -> scanner.hasNextLine()).timeout(10, TimeUnit.SECONDS).doOnError(e -> {
					System.out.println(e.getMessage());
				}).toFuture();
				if(result.get()) {
					String line = scanner.nextLine();
					source.onNext(line);
					if(line.contains("Monitor thread successfully connected to server")) {
						ready.set(true);
					}
				}
				
			});
		});
		
		serverFlow.doOnError(e -> log.warn(e.getMessage())).forEach(System.out::println);
		System.out.println("Wait to be ready");
		
		Flowable.interval(500, TimeUnit.MILLISECONDS).takeUntil(value -> !ready.get()).blockingFirst();
		System.out.println("Server is ready");		
	}
	
}
