package com.notesonjava.spanish.integration;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExternalResource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.compose.DockerComposeRule;
import com.notesonjava.commands.mongo.MongoToolsCommands;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.conjugation.ConjugationAnswerRepository;
import com.notesonjava.spanish.conjugation.ConjugationReportService;
import com.notesonjava.spanish.extensions.DockerComposeExtension;
import com.notesonjava.spanish.extensions.MongoRestoreExtension;
import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.WordStats;

import io.javalin.plugin.json.JavalinJackson;
import lombok.extern.slf4j.Slf4j;


@ExtendWith(DockerComposeExtension.class)
@ExtendWith(MongoRestoreExtension.class)
@Tag("intg")
public class ReportServiceIntg {
	
	
	private static MongoClient client;
	private static ConjugationReportService service;
	
	private static final String USER = "auth0|5bec6dda5c45c255a8544a40";
	
	
	

	@BeforeClass
	public static void init() throws IOException, InterruptedException  {
		
		PropertyMap props = PropertyLoader.loadProperties();
		String mongoHost = props.get("mongo.host").orElse("localhost");
		String mongoDb = props.get("mongo.db").orElse("answers");
		int dbPort = Integer.parseInt(props.get("db.port").orElse("27017"));
		
		client = MongoClients.create("mongodb://" + mongoHost + ":" + dbPort);
		MongoDatabase database = client.getDatabase(mongoDb);
		
		ConjugationAnswerRepository conjugationRepo = new ConjugationAnswerRepository(database);
    
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		JavalinJackson.configure(mapper);
		
		service = new ConjugationReportService(conjugationRepo);
		
		
	}
	
	@AfterClass
	public static void close() {
		client.close();
		
	}
	

	@Test
	public void dailyReport() { 
		ZonedDateTime refDate = ZonedDateTime.of(2019, 4, 3, 10, 0, 0, 0, ZoneOffset.UTC);
		List<WordStats<LocalDate>> stats = service.getDailyReport(USER, refDate).toList().blockingGet();
		Assertions.assertThat(stats).hasSize(1);
		Assertions.assertThat(stats.get(0).getValidAnswers()).isGreaterThan(0);
	}
	
	@Test
	public void wordReport() { 
		List<WordStats<Conjugation>> stats = service.conjugationReport(USER).toList().blockingGet();
		Assertions.assertThat(stats).hasSize(10);
	}

	@Test
	public void tiempoReport() { 
		List<WordStats<Tiempo>> stats = service.conjugationTiempoReport(USER).toList().blockingGet();
		Assertions.assertThat(stats).hasSize(1);
	}

}
